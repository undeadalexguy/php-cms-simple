Welcome to my simple CMS project! This was a tutorial project I completed, based on the book 'PHP for Absolute Beginners'. I am therefore hosting this code but do not own the rights to it nor purport to do so! I am hosting this repo publicly for anyone wishing to try out this simple PHP CMS project!

Steps for intallation:
1) Download/clone the repo to the target directory
1a) Ensure that your target hosting environment has Apache2, Mysql, and PHP installed and properly configured.
2)Using your tool of choice (I recommend phpmyadmin) import the file simple_blog.sql to your Mysql installation
3)Update the admin.php and index.php files for: $dbInfo, $dbUser, $dbPassword to your configuration.
4)In order to log into the backin (admin) you will need to first manually create a row in the  admin table (email and password).
5) Start using the blog system!

I may update this CMS but I also may not, as I am moving onto a similiar, but more complex, framework from scratch. If you find any issues with this framework please free to contact me. 

NOTE: the database uses the now defunct MD5 encryption method. Please note that user passwords will not be inherently secure! Use this encryption at your own risk!!!
